ROBJ =	librlock.o
OBJS =	rlock.o
DEBUG =	-g 
CFLAGS =	$(DEBUG) -I. -Ilist -Wall -fpic -Wl,-export-dynamic
LDFLAGS =	-Llist 
LDLIBS =	-llist 

RLOCK =		librlock.a
RLOCKSO = 	librlock.so
RLOCKSOV = 	$(RLOCKSO).1
RLOCKSOVL = 	$(RLOCKSOV).0.0

LN =		/bin/ln

all:		list/list.a test

list/list.a:
	(cd list; make)

test: 		$(RLOCKSOVL)

$(RLOCK):	$(OBJS)
	ld -r -o $(ROBJ) $(OBJS) $(LDFLAGS) $(LDLIBS)
	ar -crs $@ $(ROBJ)
	ranlib $@

$(RLOCKSOVL): $(RLOCK)
	gcc -shared -Wl,-soname,$(RLOCKSOV) -o $@ $(OBJS) $(LDFLAGS) $(LDLIBS)
	$(LN) -fs $(RLOCKSOVL) $(RLOCKSOV)
	$(LN) -fs $(RLOCKSOV) $(RLOCKSO)

clean:
	rm -rf  $(RLOCK) $(RLOCKSO) $(RLOCKSOV) $(RLOCKSOVL) \
		test a.out links *.o
