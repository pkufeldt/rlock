#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>

#include "rlock.h"
#include "list.h"

/* Private types */
typedef struct rangelock {
	LIST 		*rl_l;		/* list of locked ranges */
	pthread_mutex_t	rl_m;		/* mutex protecting the range lock */
	pthread_cond_t	rl_cv;		/* condition variable for waiting */
} rangelock_t;

typedef struct range {
	pthread_t	r_tid;		/* Range owner */
	size_t		r_start;	/* Range start */
	size_t		r_end;		/* Range end */
} range_t;

/*
 * Create a new lock 
 */
int
rlock_create(rlock_t *rl)
{
	rangelock_t *rlock;
	int rc;

	rlock = (rangelock_t *)malloc(sizeof(rangelock_t));
	if (!rlock) {
		errno = ENOMEM;
		return(RL_ERR);
	}

	rc = pthread_mutex_init(&rlock->rl_m, NULL);
	rc = rc || pthread_cond_init(&rlock->rl_cv, NULL);
	rlock->rl_l = list_create();
	if (rc || rlock->rl_l == NULL) {
		/* if rc, pthreads already set errno */
		
		if (rlock->rl_l) {
			errno = ENOMEM;
			list_destroy(rlock->rl_l, (void *)LIST_NODEALLOC);
		}
		free(rlock);
		return(RL_ERR);
	}

	*rl = (rlock_t)rlock;
	
	return(RL_OK);
}

/*
 * Destroy the lock. Only destroy if no ranges are locked.
 */
int
rlock_destroy(rlock_t *rl)
{
	rangelock_t *rlock = (rangelock_t *)*rl;
	
	if (!rlock) {
		errno = EINVAL;
		return(RL_ERR);
	}

	pthread_mutex_lock(&rlock->rl_m);
	
	/* A quick check to see if there were racing destroys */
 	if (!*rl) {
		pthread_mutex_unlock(&rlock->rl_m);
		errno = EINVAL;
		return(RL_ERR);
	}

	/* 
	 * Only destroy if unused, that is no locked ranges 
	 * Assuming that no locked ranges means no waiting threads 
	 */
	if (list_size(rlock->rl_l)) {
		/* still locked ranges */
		pthread_mutex_unlock(&rlock->rl_m);
		errno = EINVAL;
		return(RL_ERR);
	}

	/* 
	 * Time to destroy
	 * First set passed in rl to null to prevent any future use 
	 * Free up the list
	 */
	*rl = NULL;
	
	list_destroy(rlock->rl_l, (void *)LIST_NODEALLOC);
	pthread_mutex_unlock(&rlock->rl_m);
	free(rlock);
	return(RL_OK);
}

/*
 * List helper function to find an overlapping range.
 * If no match, return LIST_TRUE to continue searching
 * Once a match is found return FALSE to terminate search
 */
static list_boolean_t
rl_rangeoverlap(void *data, void *ldata)
{
	list_boolean_t match = LIST_TRUE;
	range_t *r = (range_t *)data;
	range_t *lr = *(range_t **)ldata;

	if (((r->r_start >= lr->r_start) && (r->r_start <= lr->r_end)) ||
	    ((r->r_end >= lr->r_start) && (r->r_end <= lr->r_end)) ) 
		match = LIST_FALSE;  /* See comment above */
	return (match);

}


/*
 * List helper function to find a given range.
 * If no match, return LIST_TRUE to continue searching
 * Once a match is found return FALSE to terminate search
 */
static list_boolean_t
rl_rangematch(void *data, void *ldata)
{
	list_boolean_t match = LIST_TRUE;
	range_t *r = (range_t *)data;
	range_t *lr = *(range_t **)ldata;

	if ((r->r_tid == lr->r_tid)    &&
	    (r->r_start == lr->r_start) &&
	    (r->r_end == lr->r_end)) 
		match = LIST_FALSE;  /* See comment above */
	return (match);

}


/* 
 * Lock a given range. 
 */
int
rlock_lock(rlock_t *rl, size_t start, size_t end)
{
	rangelock_t *rlock = (rangelock_t *)*rl;
	range_t *r;
	int rc;
	
	if (!rlock) {
		errno = EINVAL;
		return(RL_ERR);
	}

	pthread_mutex_lock(&rlock->rl_m);
	
	r = (range_t *)malloc(sizeof(range_t));
	if (!r) {
		pthread_mutex_unlock(&rlock->rl_m);
		errno = ENOMEM;
		return(RL_ERR);
	}

	/* Setup the range */
	r->r_tid = pthread_self();
	r->r_start = start;
	r->r_end = end;

	/* Loop until range is free */
	while (1) {
		rc = list_traverse(rlock->rl_l, r, rl_rangeoverlap, LIST_ALTR);
		if ((rc == LIST_EXTENT) || (rc == LIST_EMPTY)) {
			/* No range found, grant the lock */
			break;
		}

		/* 
		 * Requested range overlaps with a locked range
		 * Sleep, waiting for an unlock to release the matched
		 * range
		 */
		pthread_cond_wait(&rlock->rl_cv, &rlock->rl_m);

		/* Something signalled us to check again */
	}

	(void)list_mvrear(rlock->rl_l);
	/* Just preserve a copy of the ptr on the list */
	list_insert_after(rlock->rl_l, &r, sizeof(range_t *));
	
	errno = 0;
	pthread_mutex_unlock(&rlock->rl_m);
	return(RL_OK);
}


/* 
 * Unlock a given range. 
 */
int
rlock_unlock(rlock_t *rl, size_t start, size_t end)
{
	rangelock_t *rlock = (rangelock_t *)*rl;
	range_t r = {pthread_self(), start, end};
	range_t **rp;
	int rc;
	
	if (!rlock) {
		errno = EINVAL;
		return(RL_ERR);
	}

	pthread_mutex_lock(&rlock->rl_m);

	rc = list_traverse(rlock->rl_l, &r, rl_rangematch, LIST_ALTR);
	if ((rc == LIST_EXTENT) || (rc == LIST_EMPTY)) {
		/* 
		 * either range didn't match bounds or range isn't
		 * owned by us 
		 */
		errno = ENOLCK;
		pthread_mutex_unlock(&rlock->rl_m);
		return(RL_ERR);
	}

	/* Found the range, free it up */
	rp = (range_t **)list_remove_curr(rlock->rl_l);
	free(*rp);
	free(rp); /* list package allocated a ptr I must free */

	/* 
	 * Wake up all waiting for a range.
	 */
	pthread_cond_broadcast(&rlock->rl_cv);

	pthread_mutex_unlock(&rlock->rl_m);
	
	return(RL_OK);
}
