#ifndef _RLOCK_H
#define _RLOCK_H

/* 
 * Range Lock Library 
 *
 * This is a simple, un-optimized library that implements a range
 * lock primitive. A single lock can simultaneously lock many 
 * non-overlapping ranges, but will wait when trying to lock a range
 * that overlaps an existing locked range.
 *
 * API
 * 
 * rlock_create
 * rlock_destroy
 * rlock_lock
 * rlock_unlock
 *
 * A range lock must first be created before ranges can be locked. Each
 * call to create will create a new and unique lock. After creating a lock
 * individual ranges can be locked using that lock. Once
 * a lock is no longer useful it can be destroyed returning its resources. 
 * However a lock cannot be destroyed until all the ranges that have been 
 * locked have been unlocked. 
 * 
 * Locking a range requires a call to rlock_lock, providing a valid lock
 * and defining range to be locked (start, end).  rlock_lock only returns
 * once the lock is granted. If a range is already locked that overlaps 
 * with range lock request, the thread will sleep until such time the lock
 * can be granted. 
 *
 * Releasing a locked range requires a call to rlock_unlock. This call 
 * requires a valid lock and the range to unlock (start,end). The range 
 * provided must exactly match an existing locked range and that range must 
 * owned by the calling thread.
 */ 

/* Opaque rlock type */
typedef void * rlock_t;

int rlock_create(rlock_t *rl);
int rlock_destroy(rlock_t *rl);
int rlock_lock(rlock_t *rl, size_t start, size_t end);
int rlock_unlock(rlock_t *rl, size_t start, size_t end);

#define RL_ERR -1
#define RL_OK   0

#endif /* _RLOCK_H */
