#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <time.h>

#include "rlock.h"

/* Very simple test progam for the rlock library */

rlock_t rl;

/* Define test ranges to lock, each range will create a thread which locks it */
struct testrange {
	pthread_t tid;
	size_t start;
	size_t end;
} trs[] = {
	{0, 0, 5},
	{0, 4, 7},
	{0, 6, 10},
	{0, 15,20},
};

time_t t; /* used for timestamps */

void *
lockit(void *arg)
{
	struct testrange *tr = (struct testrange *)arg;

	rlock_lock(&rl, tr->start, tr->end);
	printf("TS: %ld: TID %lu: locked [%lu, %lu]\n",
	       time(NULL) - t, pthread_self(),tr->start,tr->end); 
	sleep(5);
	printf("TS %ld: TID %lu: unlocked [%lu, %lu]\n",
	       time(NULL) - t, pthread_self(),tr->start,tr->end); 
	rlock_unlock(&rl, tr->start, tr->end);
	pthread_exit(0);
}


int
main(int argc, char *argv[])
{
	int n =  sizeof(trs) / sizeof(trs[0]);
	int i, rc;
	void *res;
	t = time(NULL);
	
	if (rlock_create(&rl) == RL_ERR) {
		perror("init");
		exit(1);
	}

	/* Create the threads */
	for (i=0; i<n; i++) {
		printf("Creating thread %d to lock range [%lu, %lu]\n",
		       i, trs[i].start, trs[i].end);
		rc = pthread_create(&trs[i].tid, NULL, lockit, &trs[i]);
		if (rc) {
			perror("pthread_create");
		}
	}

	/* Reap the threads as they exit */
	for (i=0; i<n; i++) {
		printf("Reaping thread %d\n", i);
		rc = pthread_join(trs[i].tid, &res);
	}
	
	
	
	if (rlock_destroy(&rl) == RL_ERR) {
		perror("destroy");
		exit(1);
	}

	/* Test multiple destroys */
	if (rlock_destroy(&rl) == RL_ERR) {
		perror("expected destroy failure");
		exit(1);
	}
}
