# rlock

An exercise to implement a simple range lock library given to 
me by Norbert Kusters of AWS. 

## Compiling

To compile and run:
```
git clone https://gitlab.com/pkufeldt/rlock.git
cd rlock
git clone https://github.com/pkufeldt/list.git
make
export LD_LIBRARY_PATH=.
./test

```
